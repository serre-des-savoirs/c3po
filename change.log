# Changelog
All the changes in the ontology are documented in this file

## [1.0.0] - 22-09-2022

## Planting mode update

# Plot
### Added
    - Class : CultivableArea
    - Property : isPartOf (Domain : CultivableArea; Range : CultivableArea || CultivableRow)
    - Property : hasHeight (Domain : CultivableMound; Range : Length)

### Deleted
    - Class : Mandala
    - Property : isPartOfCultivablePlot, hasCultivatedBedPart
    - Subclass : c3poparam:Suraface subClassOf prov:Entity

### Modified
    - Class Name : Row -> CultivableRow
    - Subclass : CultivableRow subClassOf CultivableArea -> CultivableRow subClassOf CultivableLand
    - Class Name : Mound -> CultivableMound
    
    - Subclass : CultivableArea subClassOf LandUse -> CultivableRow subClassOf CultivableLand
    
    - Property Domain : hasLength - CultivableArea -> CultivableLand
    
    - Property Name : hasLandUsePeriod -> hasPeriod
    - Property Domain : hasPeriod - CultivableArea -> CultivableLand
    
    - PropertyName : hasFootPassWidth -> hasGap


# CropManagement
### Added
    - Class : Practice
    - ObjectProperty : 
        - hasSeedingDensity (Domain : PlantingProcess, Range : c3poparam:PlantingDensity)
        - hasPlotPlantingDensity (Domain : PlantingProcess, Range : c3poparam:PlantingDensity)
        - hasBedPlantingDensity (Domain : PlantingProcess, Range : c3poparam:PlantingDensity)
        - hasPlantingMode (Domain : CropItinerary, Range : c3povoc:PlantingMode)
        - hasBedWidth (Domain : RowPlantingProcess, Range : c3poparam : Length)
    - DataProperty :
        - thinningRate (Domain : BroadcastPlantingProcess || SeedingInRows, Range : xsd:decimal)
        - liftingRate (Domain : BroadcastPlantingProcess || SeedingInRows, Range : xsd:decimal)
        - edgeDistance (Domain : RowPlantingProcess, Range : Length)

### Deleted
    - Class : Seeding, SoilCleaning
    - ObjectProperty : hasPlantingDensity

### Modified
    - Property Domain : usesPlantMaterial - ProductionProcess -> Series
    - Property Domain : hasWorkload - FarmingPractice -> Practice
    - Subclass : MiscOperation subClassOf FarmingPractice -> MiscOperation subClassOf Practice
    - Subclass : NurserySeeding subClassOf Seeding -> NurserySeeding subClassOf FarmingPractice

    - Class Name : Broadcast -> BroadcastPlantingProcess
    - Subclass : BroadcastPlantingProcess subClassOf DirectSeeding -> BroadcastPlantingProcess subClassOf PlantingProcess
    
    - Class Name : DirectSeeding -> RowPlantingProcess
    - Subclass : RowPlantingProcess subClassOf DirectSeeding -> RowPlantingProcess subClassOf PlantingProcess

    - Property Domain : numberSeeds - Seeding -> SeedingInRows
    
    - Property Name : hasSpacingByLine -> SpacingBetweenRows
    - Property Name : hasSpacingInsideLine -> spacingInRow

    - Property Domain : SpacingBetweenRows - (SeedingInRows || Plantation) -> RowPlantingProcess
    - Property Domain : spacingInRow - (SeedingInRows || Plantation) -> RowPlantingProcess

    - Class Name : Plantation -> Planting
    - Subclass : Planting subClassOf PlantingProcess -> Planting subClassOf RowPlantingProcess

# Plant
## Plant Hierarchy
### Added
    - ObjectProperty :
		- belongsToHorticulturalCategory (Domain : CultivatedPlant, Range : c3povoc:HorticulturalCategory)

### Deleted
	- Class : FruitPlant, GreenManure, HerbalPlant, VegetablePlant
	- Property : hasFCUTaxon, belongsToUsageFamily, belongsToBotanicalFamily
	- Cultivar subClassOf CultivatedPlant

### Modified
	- Class Name : PlantFamily -> CultivatedFamily
	- Property Name : hasTaxRefTaxon -> hasTaxonName
	- Namespace & range : c3poplant:scientificName (Range : xsd:Name) -> dwc:scientificName (Range : xsd:string)
	- Namespace : c3poplant:prefCommonName/c3poplant:altCommonName -> skos:prefCommonName/skos:altCommonName

## BiologicalStep
### Modified
	- Class Name : GerminatationStage/GrowingStage/HarvestStage/StorageStep -> GerminationStep/GrowingStep/HarvestStep/StorageStep
	- Property Name : hasDistanceFromSeedingToPlant/hasDistanceFromSeedingToHarvest/hasDistanceFromPlantToHarvest -> hasDurationFromSeedingToPlant/hasDurationFromSeedingToHarvest/hasDurationFromPlantToHarvest
	- Replacement : time:Duration -> c3poparam:Duration. Min, max and default are properties of c3poparam:Duration.
	
## Rotation
### Added
	- ObjectProperty :
		- follows/precedes (Domain : Succesion, Range : (CultivatedFamily || Plant))

### Deleted
	- Property : hasFavoriteFollowing, hasNegativeFollowing, hasPositivePreceding, isBasedOn

### Modified
	- ClassName : Rotation -> Succesion
	- Property Name : hasRotation -> hasSuccession
	- Property Domain : hasSuccession - CultivatedPlant -> (CultivatedFamily || Plant)

## Association
### Deleted
	Property : hasPositiveAssociation, hasNegativeAssociation

### Modified
	- isAssociatedWith (Symetric Relation between Association & (CultivatedFamily || Plant))
	
## Effect
### Added
	- Class : Effect
	- ObjectProperty : 
		- hasEffect (Domain : (Succession || Association), Range : Effect)
		- hasNotation (Domain : Effect, Range : c3povoc:Impact)
	- DataProperty :
		- effectDescription (Domain : Effect, Range : xsd:string)

## Cultivar
### Added

### Deleted
	- Class : BiologicalTrait/Earliness/PhysicalTrait/PlantShape/PlantSize/PlantWeight/PlantColor
### Modified
	- Property Domain : dependsSeason/hasConservationCapacity/hasFrostTolerance/hasDiseaseTolerance - Cultivar -> CultivatedPlant
	- Property Domain : traitDescription - BiologicalTrait -> Cultivar
	- Property Domain : hasEarlinessDuration/hasEarlinessStage - Earliness -> Cultivar
	- Property Domain : hasWeight - PlantWeight -> Cultivar
	- Property Domain : hasPlantLength - PlantSize -> Cultivar
