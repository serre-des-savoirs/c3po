# Crop Planification and Production Process Ontology (C3PO)

Ontology describing a knowledge model for crop planification and production process

Documentation about this ontology can be found here : [https://open.elzeard.co/ontologies/c3po/](https://open.elzeard.co/ontologies/c3po/) (generated with [Widoco](https://dgarijo.github.io/Widoco/) software).

Shield : [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg